<?php

use Illuminate\Database\Seeder;
use App\Board;

class BoardListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('board_lists')->insert([
          'id' => Board::generateId(),
          'name' => 'Todo',
          'board_id' => '60716a11e09c299d',
          'order' => 8192,
      ]);
      DB::table('board_lists')->insert([
          'id' => Board::generateId(),
          'name' => 'Doing',
          'board_id' => '60716a11e09c299d',
          'order' => 8192 + 66536,
      ]);
      DB::table('board_lists')->insert([
          'id' => Board::generateId(),
          'name' => 'Done',
          'board_id' => '60716a11e09c299d',
          'order' => 8192 + (66536 * 2),
      ]);
    }
}
