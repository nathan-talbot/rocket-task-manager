<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Board;

class BoardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('boards')->insert([
          'name' => 'My Example Board',
          'id' => Board::generateId()
        ]);
        DB::table('boards')->insert([
          'name' => 'My Example Board 2',
          'id' => Board::generateId()
        ]);
    }
}
