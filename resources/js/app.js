import React from "react";
import { render } from "react-dom";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import AuthenticateUser from "./components/AuthenticateUser";
import Login from "./components/Login";
import Register from "./components/Register";
import MainNav from "./components/MainNav";
import GroupOfBoards from "./components/Board/GroupOfBoards";
import Board from "./components/Board/Board";

import { client } from "./client";

const App = () => (
  <ApolloProvider client={client}>
    <Router>
      <Switch>
        <AuthenticateUser>
          <MainNav/>
          <main className="main">
            <Route path="/" exact component={Login} />
            <Route path="/register" exact component={Register} />
            <Route path="/boards" exact component={GroupOfBoards} />
            <Route path="/boards/:id" exact component={Board} />
          </main>
        </AuthenticateUser>
      </Switch>
    </Router>
  </ApolloProvider>
);

render(<App />, document.getElementById("app"));