import ApolloClient from "apollo-boost";
import { AUTH_TOKEN_KEY, API_ENDPOINT } from './constants';
import uniqueAlphaNumericString from "./utils/uniqueAlphaNumericString";

export const defaultState = {
  loggedInUser: {
    __typename: "User",
    user_id: false,
    name: ""
  }
}

const resolvers = {
  BoardList: {
    clientId: (obj, args, context, info) => {
      const { optimisticResponse } = context;
      if (optimisticResponse && optimisticResponse.addList) {
        return optimisticResponse.addList.clientId;
      } else if (optimisticResponse && optimisticResponse.updateList) {
        return optimisticResponse.updateList.clientId;
      } else {
        return uniqueAlphaNumericString();
      }
    }
  },
  BoardCard: {
    clientId: (obj, args, context, info) => {
      const { optimisticResponse } = context;
      if (optimisticResponse && optimisticResponse.addCard) {
        return optimisticResponse.addCard.clientId;
      } else if (optimisticResponse && optimisticResponse.updateCard ) {
        return optimisticResponse.updateCard.clientId;
      } else {
        return uniqueAlphaNumericString();
      }
    }
  }
};

const typeDefs = `{
  type BoardList {
    clientId: String!
  }
  type BoardCard {
    clientId: String!
  }
}`;


export const client = new ApolloClient({
  uri: API_ENDPOINT,
  clientState: {
    defaults: defaultState,
    resolvers,
    typeDefs
  },
  request: (operation) => {
  	// Set auth token
    const token = localStorage.getItem(AUTH_TOKEN_KEY);
    if (token) {
      operation.setContext({
			  headers: {
			    authorization: token ? `Bearer ${token}` : "",
			  }
      });
    }
  }
});