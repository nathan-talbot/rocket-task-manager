import React from "react";
import { Mutation } from "react-apollo";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { GET_BOARD } from "../../graphql/queries";
import { UPDATE_LIST, UPDATE_CARD } from "../../graphql/mutations";
import reorderArrayOfItems from '../../utils/reorderArrayOfItems';

import { List } from "./List";
import { AddList } from "./AddList";


export default class GroupOfLists extends React.Component {
	constructor(props) {
		super(props);
		this.handleDragEnd = this.handleDragEnd.bind(this);
	}
	handleDragEnd(source, destination, type, callback) {
		const { boardId, lists } = this.props;
		const isList = type === 'COLUMN';
		const movingToDiffList = destination.droppableId !== source.droppableId;
		// Don't update unless something has change
		if (source.index !== destination.index || movingToDiffList) {
			let itemsToSort = null;
			let oldListIndex = 0;
			let currentListIndex = 0;
			let currentList = null;
			if (isList) {
				itemsToSort = lists;
			} else {
				currentList = lists.find((list, index) => {
					if (list.clientId === destination.droppableId) {
						currentListIndex = index;
						return true;
					}
				});
				itemsToSort = currentList.cards;
				if (movingToDiffList) {
					const oldList = lists.find((list, index) => {
						if (list.clientId === source.droppableId) {
							oldListIndex = index;
							return true;
						}
					});
					itemsToSort.push(oldList.cards[source.index]);
				}
			};
			const newItems = reorderArrayOfItems(
				itemsToSort, 
				destination.droppableId !== source.droppableId ? itemsToSort.length - 1 : source.index, 
				destination.index
			); 
			// Peform mutation
			let args = isList ? {
				variables: {
					id: newItems[destination.index].id,
					order: newItems[destination.index].order
				},
				optimisticResponse: {
					__typename: "Mutation",
					updateList: {
						__typename: "BoardList",
						...newItems[destination.index]
					}
				},
				update: store => {
					const data = store.readQuery({
						query: GET_BOARD,
						variables: {
							id: boardId
						}
					});
					data.boards[0].lists = newItems;
					store.writeQuery({ query: GET_BOARD, data });
				}
		  } : {
		  	variables: {
					id: newItems[destination.index].id,
					order: newItems[destination.index].order,
					listId: destination.droppableId !== source.droppableId ? currentList.id : undefined
				},
				optimisticResponse: {
					__typename: "Mutation",
					updateCard: {
						__typename: "BoardCard",
						...newItems[destination.index]
					}
				},
				update: store => {
					const data = store.readQuery({
						query: GET_BOARD,
						variables: {
							id: boardId
						}
					});
					if (movingToDiffList) {
						data.boards[0].lists[oldListIndex].cards.splice(source.index, 1);
					}
					data.boards[0].lists[currentListIndex].cards = newItems;
					store.writeQuery({ query: GET_BOARD, data });
				}
			};
			callback(args);
		}
	}
	render() {
		const { boardId, lists } = this.props;
		return (
			<Mutation mutation={UPDATE_LIST}>
				{updateList => (
				<Mutation mutation={UPDATE_CARD}>
					{updateCard => (
						<DragDropContext
							onDragEnd={({ source, destination, type }) => {
								const callback = type === "COLUMN" ? updateList : updateCard;
								this.handleDragEnd(source, destination, type, callback);
							}}
						>
							<Droppable
								droppableId={boardId}
								type="COLUMN"
								direction="horizontal"
							>
								{provided => (
									<div
										className="board__lists"
										ref={provided.innerRef}
										{...provided.droppableProps}
									>
										{lists.map((list, index) => (
											<List key={list.id} list={list} index={index} boardId={boardId}/>
										))}
										<AddList
											boardId={boardId}
											noLists={lists !== null && lists.length ? undefined : 1}
											endOrder={lists !== null && lists.length ? lists[lists.length - 1].order : 0}
										/>
									</div>
								)}
							</Droppable>
						</DragDropContext>
					)}
				</Mutation>
				)}
			</Mutation>	
		);
	}
}