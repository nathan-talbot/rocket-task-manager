import React from "react";
import { Draggable } from "react-beautiful-dnd";

import GroupOfCards from '../Card/GroupOfCards';

export const List = ({ boardId, list, index }) => {
	return list.clientId ? (
		<Draggable
			draggableId={list.clientId}
			isDragDisabled={!list.id}
	    index={index}
	    disableInteractiveElementBlocking
		>
			{(provided, snapshot) => (
				<div 
					className={"list" + (!list.id ? " list--disabled" : "")}
					ref={provided.innerRef}
		      {...provided.draggableProps}
	        {...provided.dragHandleProps}
				>
					<div class="list__header">
						<h4 className="list__name">{list.name}</h4>
					</div>
					<GroupOfCards boardId={boardId} droppableId={list.clientId} listId={list.id} cards={list.cards}/>
				</div>
			)}
		</Draggable>
	) : null;
}