import React from "react";
import { Mutation } from "react-apollo";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { ADD_LIST } from "../../graphql/mutations";
import { GET_BOARD } from "../../graphql/queries";
import { ORDER_VALUE_INCREMENT, FORM_VALIDATION_MESSAGES } from '../../constants';
import uniqueAlphaNumericString from "../../utils/uniqueAlphaNumericString";
import formValidationError from "../../utils/formValidationError";

const ValidationSchema = Yup.object().shape({
  name: Yup.string()
    .required(FORM_VALIDATION_MESSAGES.noBlank)
});

export const AddList = ({ lists, boardId, endOrder, noLists }) => {
	let input;
	return (
		<Mutation
			mutation={ADD_LIST}
		>
			{addList => (
					<Formik
						initialValues={{ 
							name: '', 
						}}
						validationSchema={ValidationSchema}
						onSubmit={(values, { resetForm }) => {
							const { name } = values;
							const clientId = uniqueAlphaNumericString(); // generate new client id
							const order = endOrder + ORDER_VALUE_INCREMENT;

							// Make request
							addList({ 
								variables: { 
									boardId,
									name,
									order
								},
								optimisticResponse: {
									__typename: "Mutation",
		              addList: {
		              	__typename: "BoardList",
		              	clientId,
		              	name,
		                order,
		                id: null,
		                cards: null
		              }
		            },
		            update: (store, { data: { addList } }) => {
					        const data = store.readQuery({ 
					        	query: GET_BOARD, 
					        	variables: {
					        		id: boardId
						        } 
						      });
					        data.boards[0].lists.push(addList);
					        store.writeQuery({ query: GET_BOARD, data });
					      }
							});

							// Reset form values
							resetForm();
						}}
						>
							{() => (
				        <Form className="add-list">
									<h3>New List</h3>
									<div className="field">
										<Field type="text" name="name" placeholder="Enter a title for a list..."/>
		          			<ErrorMessage name="name" render={formValidationError}/>
									</div>
									<button className="btn btn--primary" type="submit">Add List</button>
								</Form>
							)}
					</Formik>
			)}
		</Mutation>
	);
};