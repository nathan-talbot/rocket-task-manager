import React from "react";
import { Mutation } from "react-apollo";
import { Droppable } from "react-beautiful-dnd";
import reorderArrayOfItems from "../../utils/reorderArrayOfItems";

import Card from "./Card";
import AddCard from "./AddCard";

export default class GroupOfCards extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const { boardId, droppableId, listId, cards } = this.props;
		return (
			<React.Fragment>
				<Droppable droppableId={droppableId}>
					{provided => (
						<div
							className="list__cards"
							ref={provided.innerRef}
							{...provided.droppableProps}
						>
							{cards &&
								cards.map((card, index) => {
									return <Card key={card.id} index={index} card={card} listId={listId} boardId={boardId}/>;
								})}
						</div>
					)}
				</Droppable>
				<AddCard
					listId={listId}
					boardId={boardId}
					endOrder={cards !== null && cards.length ? cards[cards.length - 1].order : 0}
				/>
			</React.Fragment>
		);
	}
}