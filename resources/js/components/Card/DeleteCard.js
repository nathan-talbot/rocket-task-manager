import React from "react";
import { Mutation } from "react-apollo";
import { DELETE_CARD } from "../../graphql/mutations";
import { GET_BOARD } from "../../graphql/queries";

export const DeleteCard = ({ card, boardId, listId }) => (
	<Mutation mutation={DELETE_CARD}>
		{deleteCard => (
			<button
				className="card__util"
				onClick={e => {
					e.preventDefault();
					deleteCard({
						variables: {
							id: card.id
						},
						optimisticResponse: {
							__typename: "Mutation",
							deleteCard: {
								__typename: "BoardCard",
								id: card.id
							}
						},
						update: (store) => {
							const data = store.readQuery({
								query: GET_BOARD,
								variables: {
									id: boardId
								}
							});
							let currentListIndex = 0;
							const currentList = data.boards[0].lists.find((list, index) => {
								if (list.id === listId) {
									currentListIndex = index;
									return true;
								}
							});
							const cards = data.boards[0].lists[currentListIndex].cards.filter(
								c => {
									return c.id !== card.id;
								}
							);
							data.boards[0].lists[currentListIndex].cards = cards;
							store.writeQuery({ query: GET_BOARD, data });
						}
					});
				}}
				title="Delete"
			>
				<i className="material-icons">delete</i>
			</button>
		)}
	</Mutation>
);