import React from "react";
import { Draggable } from "react-beautiful-dnd";
import { Mutation } from "react-apollo";
import { GET_BOARD } from "../../graphql/queries";

import EditCard from "./EditCard";
import { DeleteCard } from "./DeleteCard";

export default class Card extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isEdit: false
		};
		this.toggleEdit = this.toggleEdit.bind(this);
	}
	toggleEdit(e) {
		const { isEdit } = this.state;
		if (typeof e !== 'undefined') e.preventDefault();
		this.setState({ isEdit: !isEdit });
	}
	render() {
		const { card, index, boardId, listId } = this.props;
		const { isEdit } = this.state;
		if (card.clientId) {
			return (
				<Draggable
					draggableId={card.clientId}
					isDragDisabled={!card.id}
			    index={index}
				>
					{(provided, snapshot) => (
						<div 
							className={"card" + (!card.id ? " card--disabled" : "")}
							ref={provided.innerRef}
				      {...provided.draggableProps}
			        {...provided.dragHandleProps}
						>
							<div className="card__header">
								{isEdit ? 
									<React.Fragment>
										<h6 className="card__heading">Editing</h6>
										<button className="card__util" onClick={this.toggleEdit} title="Close Edit">
											<i className="material-icons">close</i>
										</button>
										<DeleteCard {...{boardId, listId, card}}/>
									</React.Fragment>
								: 
									<React.Fragment>
										{card.id ? 
											<button className="card__util" onClick={this.toggleEdit} title="Edit">
												<i className="material-icons">edit</i>
											</button>
										: null}
									</React.Fragment>
								}
							</div>
							{isEdit ?
								<EditCard {...{boardId, listId, card}} onComplete={this.toggleEdit}/>
							:
								<div className="card__content">
									{card.color ? <span class="card__label" style={{ backgroundColor: card.color }}></span> : null}
									<h4 className="card__name">{card.name}</h4>
								</div>
							}
						</div>
					)}
				</Draggable>
			)
		} else {
			return null;
		}
	}
}