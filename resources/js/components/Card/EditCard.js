import React from "react";
import { Mutation } from "react-apollo";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { UPDATE_CARD, ADD_CARD } from "../../graphql/mutations";
import { GET_BOARD } from "../../graphql/queries";
import { ORDER_VALUE_INCREMENT, FORM_VALIDATION_MESSAGES } from '../../constants';
import uniqueAlphaNumericString from "../../utils/uniqueAlphaNumericString";
import formValidationError from "../../utils/formValidationError";

import ColorPicker from "./ColorPicker";

const defaultProps = {
	card: null,
	boardId: null,
	listId: null,
	endOrder: null,
	buttonText: "Save",
	onComplete: null
}

const ValidationSchema = Yup.object().shape({
  name: Yup.string()
    .required(FORM_VALIDATION_MESSAGES.noBlank)
});

export default class EditCard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			inputName: props.card ? props.card.name : props.inputName,
			inputColor: props.card ? props.card.color : props.inputName,
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleColorChange = this.handleColorChange.bind(this);
	}
	handleInputChange(event) {
		const target = event.target;
		let value = target.type === "checkbox" ? target.checked : target.value;
		const name = target.name;
		if (parseInt(value) == 0) {
			value = false;
		}
		this.setState({ [name]: value });
	}
	handleColorChange(value) {
		this.setState({
			inputColor: value
		});
	}
	render() {
		const { 
			card,
			boardId, 
			listId, 
			endOrder, 
			buttonText,
			onComplete
		} = this.props;
		const { 
			inputName, 
			inputColor, 
			isActive 
		} = this.state;
		return (
			<Mutation mutation={card ? UPDATE_CARD : ADD_CARD}>
				{mutate => (
					<Formik
						initialValues={{ 
							name: card ? card.name : '',
							color: card ? card.color : ''
						}}
						validationSchema={ValidationSchema}
						onSubmit={(values, { resetForm }) => {
							const { name, color } = values;
							// If a card prop exists then it must be 'update' instance
							const mutation = card ? 'updateCard' : 'addCard';
							const order = card ? card.order : endOrder + ORDER_VALUE_INCREMENT;
							const id = card ? card.id : null;
							const clientId = card ? card.clientId : uniqueAlphaNumericString();
							// Make request
							mutate({
								variables: {
									id,
									order,
									listId,
									name,
									color
								},
								optimisticResponse: {
									__typename: "Mutation",
									[mutation]: {
										__typename: "BoardCard",
										clientId,
										id,
										order,
										listId,
										name,
										color
									}
								},
								update: (store, { data: { addCard, updateCard } }) => {
									const data = store.readQuery({
										query: GET_BOARD,
										variables: {
											id: boardId
										}
									});
									let currentListIndex = 0;
									const currentList = data.boards[0].lists.find(
										(list, index) => {
											if (list.id === listId) {
												currentListIndex = index;
												return true;
											}
										}
									);
									const cards = data.boards[0].lists[currentListIndex].cards;
									if (updateCard) {
										let currentCardIndex = 0;
										cards.find(
											(c, index) => {
												if (c.clientId === updateCard.clientId) {
													currentCardIndex = index;
													return true;
												}
											}
										);
										data.boards[0].lists[currentListIndex].cards[currentCardIndex] = updateCard;
									}
									if (addCard) {
										if (cards) {
											cards.push(addCard);
										}
										data.boards[0].lists[currentListIndex].cards = cards || [addCard];
									}
									store.writeQuery({ query: GET_BOARD, data });
								}
							});
							// Reset form values
							if (!card) resetForm();
							// Fire callback
							if (typeof onComplete === 'function') onComplete();
						}}
						>
							{({ setFieldValue }) => (
				        <Form className="card-form">
									<div className="field">
										<Field 
											name="name" 
											render={({ field, form: { isSubmitting } }) => (
										    <textarea 
										    	{...field}
										    	placeholder="Enter a title for this card..."
										    	disabled={isSubmitting} 
										    	onMouseDown={e => e.stopPropagation()}
													onKeyDown={e => e.stopPropagation()}
												/>
										  )}
										/>
	          				<ErrorMessage name="name" render={formValidationError}/>
									</div>
									<div className="field">
										<Field 
											name="color" 
											render={({ field }) => {
												return <ColorPicker {...field} onChange={setFieldValue}/>;
											}}
										/>
									</div>
									<button class="btn btn--primary" type="submit">
										{buttonText}
									</button>
								</Form>
							)}
					</Formik>
				)}
			</Mutation>
		);
	}
}

EditCard.defaultProps = defaultProps;