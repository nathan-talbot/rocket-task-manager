import React from "react";

const defaultProps = {
	colors: [
				"#F8BC32",
				"#EA4535",
				"#4285F3",
				"#4FA954"
			],
	value: "",
	onChange: null
}
export default class ColorPicker extends React.Component {
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(e) {
		const { name, onChange } = this.props;
		e.preventDefault();
		e.stopPropagation()
    const target = e.target;
    const value = target.getAttribute('data-color');
    onChange(name, value);
  }
	render() {
		const { colors, name, value } = this.props;
		return (
			<div className="color-selector">
				{colors.map((color, index) => {
					return (
						<button
							data-color={color}
							key={index}
							className={"color-selector__choice" + ( color === value ? " is-active" : "" )}
							style={{backgroundColor: color}}
							onClick={this.handleChange}
						/>
					)
				})}
				<input type="hidden" name={name} value={value}/>
			</div>
		);
	}
}

ColorPicker.defaultProps = defaultProps;