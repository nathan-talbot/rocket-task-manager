import React from "react";
import EditCard from "./EditCard";

export default class AddCard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isActive: false
		};
	}
	render() {
		const { boardId, listId, endOrder } = this.props;
		const { isActive } = this.state;
		return (
			<div className="list__add-card add-card">
				{!isActive ? (
					<button
						className="add-card__open"
						onClick={() => this.setState({ isActive: true })}
					>
						+ Add card
					</button>
				) : (
					<React.Fragment>
						<EditCard 
							{...{boardId, listId, endOrder}} 
							buttonText={"Add"}
							onComplete={() => {
								this.setState({ isActive: false })
							}}
						/>
						<button
							className="add-card__close"
							onClick={() => this.setState({ isActive: false })}
							title="Close"
						>
							<i className="material-icons">close</i>
						</button>
					</React.Fragment>
				)}
			</div>
		);
	}
}