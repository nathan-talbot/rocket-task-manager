import React from "react";
import { Query, withApollo } from "react-apollo";
import { Redirect } from "react-router-dom";
import { GET_LOGGED_IN_USER } from "../graphql/queries";
import { AUTH_TOKEN_KEY } from '../constants';

class AuthenticateUser extends React.Component {
	render() {
		const { location, client, children } = this.props;
		const hasAuthToken = localStorage.getItem(AUTH_TOKEN_KEY);
		const forceLoginPage = (
			<React.Fragment>
				{location.pathname !== "/" && location.pathname !== "/register" && <Redirect to="/"/>}
				{children}
			</React.Fragment>
		)
		return (
			<React.Fragment>
				{hasAuthToken ?
					<Query 
						query={GET_LOGGED_IN_USER}
					>
					{({ loading, error, data }) => {
						if (loading) return <p>Loading...</p>;
						if (error) return <p>Error :(</p>;
						const { users } = data;
						// Store user details in apollo store
						if (users) client.writeData({ data: { loggedInUser: users[0] } });
						// Always send user to boards page if they're logged in and try to access root
						return location.pathname === "/" && users ? <Redirect to="/boards"/> : children;
					}}
					</Query>
				: forceLoginPage }
			</React.Fragment>
		)
	}
}

export default withApollo(AuthenticateUser);