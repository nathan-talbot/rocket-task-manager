import React from 'react';
import styled, { keyframes } from 'styled-components';

const spinnerKeyframes = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
}`;

const Spinner = styled.span`
    width: 20px;
    height: 20px;
    border-radius: 20px;
    border-width: 3px;
    border-style: solid;
    border-color: rgba(255, 255, 255, .4) rgba(255, 255, 255, .4) rgba(255, 255, 255, .4) #deebff;
    display: inline-block;
    vertical-align: middle;
    -webkit-animation: ${spinnerKeyframes} .8s infinite linear;
    animation: ${spinnerKeyframes} .8s infinite linear;
    margin-left: 10px;
    z-index: 2;
}`;

export const ButtonWithSpinner = ({ text, loading, disabled }) => {
	return (
		<button 
			className={"btn btn--primary" + (loading ? " btn--loading" : "")}
			disabled={disabled}
            type="submit"
		>
			<span style={{
				display: "inline-block",
    		verticalAlign: "middle"
			}}>
				{text}
			</span>
			{loading && <Spinner/>}
		</button>
	)
}