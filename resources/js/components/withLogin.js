import React from "react";
import { withApollo } from "react-apollo";
import { AUTH_TOKEN_KEY } from '../constants';
import { LOGGED_IN_USER } from '../graphql/queries';

import { defaultState } from '../client';

function withLogin(WrappedComponent) {
	return withApollo(class extends React.Component {
		constructor(props){
			super(props);
			this.setUser = this.setUser.bind(this);
			this.logoutUser = this.logoutUser.bind(this);
		}
		setUser(user, redirect = false) {
			const { history, client } = this.props;
			// Store token in local storage
			localStorage.setItem(AUTH_TOKEN_KEY, user.token);
			// Store user details in apollo store
			client.writeData({ data: { loggedInUser: user } });
			// Redirect user to boards page
			if (redirect) history.push('/boards');
		}
		logoutUser(redirect = false) {
			const { history, client } = this.props;
			// Remove auth token
			localStorage.removeItem(AUTH_TOKEN_KEY);
			// Clear apollo store
			client.clearStore()
				.then(() => {
					// Restore defaults
					client.writeData({ data: defaultState });
					// Redirect to home page
					if (redirect) history.push('/');
				});
		}
		render() {
			return (
				<WrappedComponent 
					setUser={this.setUser}
					logoutUser={this.logoutUser}
					{...this.props}
				/>
			)
		}
	})
}

export default withLogin;