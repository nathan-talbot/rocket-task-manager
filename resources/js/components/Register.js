import React from "react";
import { Mutation } from "react-apollo";
import { Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { REGISTER_USER } from "../graphql/mutations";
import { FORM_VALIDATION_MESSAGES } from '../constants';

import withLogin from "./withLogin";
import { ButtonWithSpinner } from "./common/ButtonWithSpinner";

const RegisterSchema = Yup.object().shape({
  firstName: Yup.string()
    .required(FORM_VALIDATION_MESSAGES.required),
  lastName: Yup.string()
    .required(FORM_VALIDATION_MESSAGES.required),
  email: Yup.string()
    .email(FORM_VALIDATION_MESSAGES.invalidEmail)
    .required(FORM_VALIDATION_MESSAGES.required),
 	password: Yup.string()
    .required(FORM_VALIDATION_MESSAGES.required)
});

class Register extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isError: false,
			isLoading: false
		};
		this.validationError = this.validationError.bind(this);
	}
	validationError(msg) {
		return <div className="field__error">{msg}</div>;
	}
	render() {
		const { setUser, logoutUser } = this.props;
		const { isError, isLoading } = this.state;
		return (
			<Mutation
				mutation={REGISTER_USER}
				onCompleted={(data) => {
					const { register: user } = data;
					// Check if token was sent otherwise it failed
					if (user.token) {
						// Push new user info to apollo store and redirect
						setUser(user, true);
					} else {
						this.setState({ isError: true, isLoading: false });
					}
				}}
				onError={() => this.setState({ isError: true, isLoading: false })}
			>
			{register => (
				<Formik
					initialValues={{ 
						firstName: '',
						lastName: '',
						email: '', 
						password: '' 
					}}
					validationSchema={RegisterSchema}
					onSubmit={(values) => {
						const { 
							firstName,
							lastName,
							email, 
							password
						} = values;
						// Remove any displayed error messages and set to loading
						this.setState({ isError: false, isLoading: true });
						// Perform mutation
							register({
								variables: {
									name: firstName + " " + lastName,
									email,
									password
								}
							});
					}}
				>
					{() => (
		        <Form className="register-form">
							<h2>Register</h2>
							<div className="field">
								<Field type="text" name="firstName" placeholder="First name*"/>
          			<ErrorMessage name="firstName" render={this.validationError}/>
							</div>
							<div className="field">
								<Field type="text" name="lastName" placeholder="Last name*"/>
          			<ErrorMessage name="lastName" render={this.validationError}/>
							</div>
							<div className="field">
								<Field type="text" name="email" placeholder="Email address*"/>
          			<ErrorMessage name="email" render={this.validationError}/>
							</div>
							<div className="field">
								<Field type="password" name="password" placeholder="Password*"/>
          			<ErrorMessage name="password" render={this.validationError}/>
							</div>
							<ButtonWithSpinner text="Register" loading={isLoading} disabled={isLoading}/>
							{isError && (
								<div className="dialog dialog--error">
									<p>Oops! Something went wrong, please try again.</p>
								</div>
							)}
						</Form>
					)}
				</Formik>
				)}
			</Mutation>
		);
	}
}

export default withLogin(Register);