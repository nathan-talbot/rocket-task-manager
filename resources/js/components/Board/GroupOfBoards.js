import React from 'react';
import { Query, withApollo } from 'react-apollo';
import { Link } from 'react-router-dom';
import { GET_BOARDS, LOGGED_IN_USER } from '../../graphql/queries';

import AddBoard from './AddBoard';

class GroupOfBoards extends React.Component {
	render() {
		const { client, history } = this.props;
		const { loggedInUser: { user_id } } = client.readQuery({ query: LOGGED_IN_USER });
		return (
			<div className="boards">
				<h2>My Boards</h2>
				<Query 
					query={GET_BOARDS}
					variables={{ user_id }}
				>
					{({ loading, error, data }) => {
						if (loading) return <p>Loading...</p>;
	      		if (error) return <p>Error :(</p>;
	      		const { boards } = data;
						return (
							<React.Fragment>
								<ul class="boards__list">
									{boards.map((board) => {
										return <li className="boards__item" key={board.id}><Link to={"/boards/" + board.id} className="boards__tile">{board.name}</Link></li>
									})}
								</ul>
								<AddBoard history={history}/>
							</React.Fragment>
						)
					}}
				</Query>
			</div>
		)
	}
}

export default withApollo(GroupOfBoards);