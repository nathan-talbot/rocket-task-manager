import React from "react";
import { Query } from "react-apollo";
import { GET_BOARD } from '../../graphql/queries';

import GroupOfLists from '../List/GroupOfLists';
import { ErrorPage } from '../common/ErrorPage';

export default class Board extends React.Component {
	render() {
		const { match } = this.props;
		return (
			<Query 
				query={GET_BOARD}
				variables={{
					id: match.params.id
				}}
				notifyOnNetworkStatusChange={true} 
			>
				{({ loading, error, data }) => {
					if (loading) return <p>Loading...</p>;
					if (error) return <ErrorPage error={error}/>;
					const { boards } = data;
					if (boards) {
						const board = boards[0];
						return (
							<div className="board">
								<h2>{board.name}</h2>
								<GroupOfLists boardId={board.id} lists={board.lists}/>
							</div>
						);
					} else {
						return <p>Error :(</p>;
					}
				}}
			</Query>
		);
	}
}