import React from "react";
import { Mutation, withApollo } from "react-apollo";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { ADD_BOARD } from "../../graphql/mutations";
import { GET_BOARDS, LOGGED_IN_USER } from "../../graphql/queries";
import { FORM_VALIDATION_MESSAGES } from '../../constants';
import uniqueAlphaNumericString from "../../utils/uniqueAlphaNumericString";
import formValidationError from "../../utils/formValidationError";

import { ButtonWithSpinner } from "../common/ButtonWithSpinner";

const ValidationSchema = Yup.object().shape({
  name: Yup.string()
    .required(FORM_VALIDATION_MESSAGES.noBlank)
});

class AddBoard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isError: false,
			isLoading: false
		};
	}	
	render() {
		const { client, history } = this.props;
		const { isLoading } = this.state;
		return (
			<Mutation
				mutation={ADD_BOARD}
			>
				{addBoard => (
					<Formik
						initialValues={{ 
							name: '', 
						}}
						validationSchema={ValidationSchema}
						onSubmit={(values, { resetForm }) => {
							const { name } = values;
							const { loggedInUser: { user_id } } = client.readQuery({ query: LOGGED_IN_USER });

							// Set loading state
							this.setState({ isLoading: true });

							// Make request
							addBoard({ 
								variables: { 
									name,
									user_id
								},
		            update: (store, { data: { addBoard } }) => {
			              const data = store.readQuery({ 
			              	query: GET_BOARDS,
			              	variables: { user_id }
			              });

			              // Add to existing array of boards or create new one
			              if (data.boards) {
			              	data.boards.push(addBoard);
			              } else {
			              	data.boards = [addBoard];
			              }

			              client.writeQuery({ query: GET_BOARDS, variables: { user_id }, data });
			              // Redirect to board once created
			              history.push('/boards/' + addBoard.id);
		            }
							});

							// Reset form
							resetForm();
						}}
					>
						{() => (
			        <Form className="add-board">
								<h3>New Board</h3>
								<div className="field">
									<Field type="text" name="name" placeholder="Enter a title for the board..."/>
	          			<ErrorMessage name="name" render={formValidationError}/>
								</div>
								<ButtonWithSpinner text="Add" loading={isLoading} disabled={isLoading}/>
							</Form>
						)}
					</Formik>
				)}
			</Mutation>
		);
	}
};

export default withApollo(AddBoard);