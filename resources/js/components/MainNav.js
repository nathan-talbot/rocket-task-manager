import React from "react";
import { Mutation, withApollo } from "react-apollo";
import { Link, withRouter } from "react-router-dom";
import { AUTH_TOKEN_KEY } from '../constants';
import { LOGGED_IN_USER } from '../graphql/queries';

import withLogin from "./withLogin";

const MainNav = ({ client, history, logoutUser }) => {
	const { loggedInUser: { user_id, name: user_name } } = client.readQuery({ query: LOGGED_IN_USER });
	const isLoggedIn = user_id;
	return (
		<header className="header">
			<h1 className="header__logo">
				<i className="material-icons">event_note</i> <span>Rocket Task Manager</span>
			</h1>
			{isLoggedIn &&
				<div className="header__right">
					<div className="user-greeting">
						Hello{user_name && " " + user_name}! <a href="#" onClick={
							(e) => {
								e.preventDefault();
								logoutUser(true);
							}
						}>Logout</a>
					</div>
				</div>
			}
			<nav className="navbar">
				{isLoggedIn ?
					<ul className="navbar__list">
						<li className="navbar__item">
							<Link to="/boards" className="navbar__link">
								My Boards
							</Link>
						</li>
					</ul>
				:
					<ul className="navbar__list">
						<li className="navbar__item">
							<Link to="/" className="navbar__link">
								Home
							</Link>
						</li>
						<li className="navbar__item">
							<Link to="/register" className="navbar__link">
								Register
							</Link>
						</li>
					</ul>
				}
			</nav>
		</header>
	)
};

export default withRouter(withLogin(MainNav));