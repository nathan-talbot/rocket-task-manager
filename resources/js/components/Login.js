import React from "react";
import { Mutation } from "react-apollo";
import { Link } from "react-router-dom";
import { LOGIN_USER } from "../graphql/mutations";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { FORM_VALIDATION_MESSAGES } from '../constants';

import withLogin from "./withLogin";
import { ButtonWithSpinner } from "./common/ButtonWithSpinner";

const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email(FORM_VALIDATION_MESSAGES.invalidEmail)
    .required(FORM_VALIDATION_MESSAGES.required),
 	password: Yup.string()
    .required(FORM_VALIDATION_MESSAGES.required)
});

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isError: false,
			isLoading: false
		};
		this.validationError = this.validationError.bind(this);
	}
	validationError(msg) {
		return <div className="field__error">{msg}</div>;
	}
	render() {
		const { setUser, logoutUser } = this.props;
		const { isError, isLoading } = this.state;
		return (
			<Mutation
				mutation={LOGIN_USER}
				onCompleted={(data) => {
					const { login: user } = data;
					// Check if token was sent otherwise it failed
					if (user.token) {
						// Push new user info to apollo store and redirect
						setUser(user, true);
					} else {
						this.setState({ isError: true, isLoading: false });
					}
				}}
				onError={() => this.setState({ isError: true, isLoading: false })}
			>
			{login => (
				<React.Fragment>
					<Formik
						initialValues={{ 
							email: '', 
							password: '' 
						}}
						validationSchema={LoginSchema}
						onSubmit={(values) => {
							const { email, password } = values;
							// Remove any displayed error messages and set to loading
								this.setState({ isError: false, isLoading: true });
								// Perform mutation
								login({
									variables: {
										email,
										password
									}
								});
						}}
					>
						{() => (
			        <Form className="login-form">
								<h2>Login</h2>
								<div className="field">
									<Field type="text" name="email" placeholder="Email address*"/>
	          			<ErrorMessage name="email" render={this.validationError}/>
								</div>
								<div className="field">
									<Field type="password" name="password" placeholder="Password*"/>
	          			<ErrorMessage name="password" render={this.validationError}/>
								</div>
								<ButtonWithSpinner text="Login" loading={isLoading} disabled={isLoading}/>
								{isError && (
									<div className="dialog dialog--error">
										<p>Your email address or password is incorrect.</p>
									</div>
								)}
							</Form>
						)}
					</Formik>
					<p>Are you new here? <Link to="/register">Register an account</Link></p>
				</React.Fragment>
				)}
			</Mutation>
		);
	}
}

export default withLogin(Login);