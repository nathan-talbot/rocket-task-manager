export const AUTH_TOKEN_KEY = 'auth-token';
export const API_ENDPOINT = process.env.NODE_ENV === 'development' ? 'http://rockstar.localhost/graphql' : 'https://rocket-tasks.naashdev.net/graphql';
export const ORDER_START_VALUE = 8192;
export const ORDER_VALUE_INCREMENT = ORDER_START_VALUE * 8;
export const FORM_VALIDATION_MESSAGES = {
	required: 'This field is required.',
	noBlank: 'This can\'t be blank!',
	invalidEmail: 'This email address is invalid.'
}