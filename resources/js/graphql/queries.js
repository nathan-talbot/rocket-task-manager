import gql from "graphql-tag";

// App State
export const LOGGED_IN_USER = gql`{
	loggedInUser @client {
		user_id
		name
	}
}`;

// Users
export const GET_LOGGED_IN_USER = gql`{
	users {
		user_id
		name
	}
}`;

//Boards
export const GET_BOARDS = gql`
	query Boards($user_id: String!) {
		boards(user_id: $user_id) {
			id
	    name
	    user_id
	  }
	}
`;

export const GET_BOARD = gql`
	query Boards($id: String!) {
		boards(id: $id) {
			id
	    name
	    lists {
	    	clientId @client
	    	id
	    	name
	    	order
	    	cards {
	    		clientId @client
	    		id
	    		name
	    		order
	    		color
	    	}
	    }
	  }
	}
`;