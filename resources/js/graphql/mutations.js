import gql from "graphql-tag";

// User
export const LOGIN_USER = gql`
mutation login($email: String!, $password: String!) {
  login(email:$email, password: $password) {
    token
    user_id
    name
  }
}
`;

export const REGISTER_USER = gql`
mutation register($email: String!, $password: String!, $name: String!) {
  register(email:$email, password: $password, name: $name) {
    token
    user_id
    name
  }
}
`;


// Boards
export const ADD_BOARD = gql`
	mutation addBoard($name: String!, $user_id: String!) {
		addBoard(name: $name, user_id: $user_id) {
			id
			name
			user_id
		}
	}
`;

// Lists
export const ADD_LIST = gql`
	mutation addList($boardId: String!, $name: String!, $order: Int!) {
		addList(boardId: $boardId, name: $name, order: $order) {
			id
			name
			order
			clientId @client
			cards {
				id
				name
				order
				color
				clientId @client
			}
		}
	}
`;

export const UPDATE_LIST = gql`
	mutation updateList($id: String!, $order: Int!) {
		updateList(id: $id, order: $order) {
			id
			name
			order
			clientId @client
		}
	}
`;

// Cards
export const ADD_CARD = gql`
	mutation addCard($listId: String!, $name: String, $order: Int!, $color: String) {
		addCard(listId: $listId, name: $name, order: $order, color: $color) {
			id
			name
			order
			color
			clientId @client
		}
	}
`;

export const UPDATE_CARD = gql`
	mutation updateCard($id: String!, $order: Int, $listId: String, $color: String, $name: String) {
		updateCard(id: $id, order: $order, listId: $listId, color: $color, name: $name) {
			id
			name
			order
			color
			clientId @client
		}
	}
`;

export const DELETE_CARD = gql`
	mutation deleteCard($id: String) {
		deleteCard(id: $id) {
			id
		}
	}
`;