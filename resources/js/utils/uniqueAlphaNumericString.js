export default function uniqueAlphaNumericString() {
	return Math.random().toString(36).slice(2);
}