import { ORDER_VALUE_INCREMENT } from '../constants';

export default function reorderArrayOfItems(array, oldIndex, newIndex) {
	console.log(array);

	const [removedItem] = array.splice(oldIndex, 1);

	array.splice(newIndex, 0, removedItem);

	const prevPosition = newIndex ? array[newIndex - 1].order : 0;
	const nextPosition =
		newIndex + 1 !== array.length ? array[newIndex + 1].order : 0;
	let newPosition = prevPosition
		? prevPosition + (nextPosition - prevPosition) * 0.5
		: nextPosition * 0.5;
	newPosition = !nextPosition ? prevPosition + ORDER_VALUE_INCREMENT : newPosition;

	array[newIndex].order = newPosition;

	return array;
}