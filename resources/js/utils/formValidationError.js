import React from "react";

export default function validationError(msg) {
	return <div className="field__error">{msg}</div>;
}