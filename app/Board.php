<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
	protected $primaryKey = 'id';
  public $incrementing = false;

	public static function generateId() {
		return bin2hex(random_bytes(8));
	}

	public function user() 
  {
  	return $this->belongsTo('App\User');
  }

  public function lists() 
  {
  	return $this->hasMany('App\BoardList');
  }
}
