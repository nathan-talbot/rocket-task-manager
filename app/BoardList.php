<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardList extends Model
{
	protected $primaryKey = 'id';
  public $incrementing = false;

  public function board() 
  {
  	return $this->belongsTo('App\Board');
  }

  public function cards() 
  {
  	return $this->hasMany('App\BoardCard');
  }
}
