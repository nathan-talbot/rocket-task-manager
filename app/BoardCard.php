<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardCard extends Model
{
	protected $primaryKey = 'id';
  public $incrementing = false;
  
  public function list() 
  {
  	return $this->belongsTo('App\BoardList');
  }
}
