<?php 

namespace App\GraphQL\Query;

use GraphQL;
use App\User;
use App\Board;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Tymon\JWTAuth\Facades\JWTAuth;

class BoardsQuery extends Query {

  private $auth;

	protected $attributes = [
    'name' => 'boards'
  ];

  public function type()
  {
  	return Type::listOf(GraphQL::type('Board'));
  }

  public function args()
  {
    return [
      'user_id' => ['name' => 'user_id', 'type' => Type::string()],
      'id' => ['name' => 'id', 'type' => Type::string()],
    ];
  }

  public function authorize(array $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    return (boolean) $this->auth;
  }

  public function resolve($root, $args)
  {
    $boards = null;
    $auth_user = JWTAuth::toUser( JWTAuth::parseToken() );

    // Single
    if ( isset($args['id']) ) {
      $boards = Board::where('id' , $args['id'])->get();
      $boards = $auth_user->user_id == $boards[0]->user_id ? $boards : null; // must match auth user
    // All
    } elseif ( isset($args['user_id']) ) {
      if ($auth_user->user_id == $args['user_id']) { // must match auth user
        $boards = Board::where('user_id' , $args['user_id'])->get();
      }
    }
    
    // Sort lists and cards by order
    if ($boards) {
      foreach($boards as $board) {
        $lists = collect($board->lists);
        foreach($lists as $list) {
          $cards = collect($list->cards);
          $list->cards = $cards->sortBy('order');
        }
        $board->lists = $lists->sortBy('order');
      }
    }

    return $boards;
  }
}