<?php 

namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsersQuery extends Query {

  private $auth;

	protected $attributes = [
    'name' => 'users'
  ];

  public function type()
  {
  	return Type::listOf(GraphQL::type('User'));
  }

  public function args()
  {
    return [];
  }

  public function resolve($root, $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    if ( $this->auth ) {
      $user = JWTAuth::toUser( JWTAuth::parseToken() );
      return [[
        'user_id' => $user->user_id,
        'name' => $user->name
      ]];
    } else {
      return null;
    }
    
  }
}