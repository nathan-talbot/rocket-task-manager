<?php
namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType 
{

	protected $attributes = [
		'name' => 'User'
	];

	public function fields() {
		return [
			'user_id' => [
				'type' => Type::string()
			],
			'name' => [
				'type' => Type::string()
			],
			'token' => [
				'type' => Type::string()
			]
		];
	}

}