<?php
namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class BoardListType extends GraphQLType 
{
	protected $attributes = [
		'name' => 'BoardList',
		'description' => 'A list of tasks'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::string()),
        'description' => 'A unique identifier'
			],
			'board' => [
        'type' => Type::nonNull(GraphQL::type('Board')),
        'description' => 'The board the list belongs to'
      ],
			'name' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The lists name'
			],
			'order' => [
				'type' => Type::nonNull(Type::int()),
        'description' => 'The order the list should be in'
			],
			'cards' => [
				'type' => Type::listOf(GraphQL::type('BoardCard')),
				'description' => 'The lists cards'
			]
		];
	}

}