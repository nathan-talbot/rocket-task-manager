<?php
namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class BoardType extends GraphQLType 
{

	protected $attributes = [
		'name' => 'Board',
		'description' => 'A board with lists of tasks'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::nonNull(Type::string()),
        'description' => 'A unique identifier'
			],
			'user_id' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The user the board belongs to'
			],
			'name' => [
				'type' => Type::nonNull(Type::string()),
				'description' => 'The boards name'
			],
			'lists' => [
				'type' => Type::listOf(GraphQL::type('BoardList')),
				'description' => 'The boards lists'
			]
		];
	}

}