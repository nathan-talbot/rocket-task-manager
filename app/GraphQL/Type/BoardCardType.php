<?php
namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class BoardCardType extends GraphQLType 
{
	protected $attributes = [
		'name' => 'BoardCard',
		'description' => 'A card with a task'
	];

	public function fields() {
		return [
			'id' => [
				'type' => Type::string(),
        'description' => 'A unique identifier'
			],
			'name' => [
				'type' => Type::string(),
				'description' => 'The cards name'
			],
			'color' => [
				'type' => Type::string(),
				'description' => 'The card color'
			],
			'order' => [
				'type' => Type::nonNull(Type::int()),
        'description' => 'The order the card should be in'
			],
		];
	}

}