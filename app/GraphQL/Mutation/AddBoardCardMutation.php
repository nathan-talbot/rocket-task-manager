<?php 

namespace App\GraphQL\Mutation;

use GraphQL;
use App\Board;
use App\BoardCard;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class AddBoardCardMutation extends Mutation
{
	protected $attributes = [
    'name' => 'addCard'
  ];

  public function type()
 	{
  	return GraphQL::type('BoardCard');
  }

  public function args()
  {
    return [
      'name' => ['name' => 'name', 'type' => Type::string()],
      'order' => ['name' => 'order', 'type' => Type::nonNull(Type::int())],
      'color' => ['name' => 'color', 'type' => Type::string()],
      'listId' => ['name' => 'listId', 'type' => Type::nonNull(Type::string())],
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'order' => ['required'],
      'listId' => ['required']
    ];
  }

  public function authorize(array $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    return (boolean) $this->auth;
  }

  public function resolve($root, $args)
  {
  	$card = new BoardCard;
    $card->id = Board::generateId();
   	if (isset($args['name'])) {
   		$card->name = $args['name'];
   	}
   	if (isset($args['color'])) {
   		$card->color = $args['color'];
   	}
   	$card->order = $args['order'];
    $card->board_list_id = $args['listId'];
  	$card->save();

  	return BoardCard::find($card->id);
  }
}