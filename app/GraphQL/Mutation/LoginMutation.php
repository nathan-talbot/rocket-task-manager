<?php 

namespace App\GraphQL\Mutation;

use Auth;
use JWTAuth;
use GraphQL;
use App\User;
use Illuminate\Support\Facades\Hash;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginMutation extends Mutation
{
	protected $attributes = [
    'name' => 'login'
  ];

  public function type()
 	{
  	return GraphQL::type('User');
  }

  public function args()
  {
    return [
      'email' => ['name' => 'email', 'type' => Type::string()],
      'password' => ['name' => 'password', 'type' => Type::string()],
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'email' => ['required'],
      'password' => ['required'],
    ];
  }

  public function resolve($root, $args)
  {
  	$credentials = [
  		'email' => $args['email'], 
  		'password' => $args['password']
  	];
  	
    try {
      $token = JWTAuth::attempt($credentials);
    } catch (\JWTException $e) {
      $token = null;
    }

		if ($token != false) {
			$user = Auth::user();
			$user_id = $user->user_id;
      $user_name = $user->user_name;
		} else {
			$token = null;
			$user_id = null;
      $user_name = null;
		}
  	return [
      'token' => $token, 
      'user_id' => $user_id,
      'name' => $user_name
    ];
  }
}