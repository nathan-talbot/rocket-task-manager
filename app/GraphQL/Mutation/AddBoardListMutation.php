<?php 

namespace App\GraphQL\Mutation;

use GraphQL;
use App\Board;
use App\BoardList;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class AddBoardListMutation extends Mutation
{
	protected $attributes = [
    'name' => 'addList'
  ];

  public function type()
 	{
  	return GraphQL::type('BoardList');
  }

  public function args()
  {
    return [
      'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string())],
      'order' => ['name' => 'order', 'type' => Type::nonNull(Type::int())],
      'boardId' => ['name' => 'boardId', 'type' => Type::nonNull(Type::string())],
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'name' => ['required'],
    ];
  }

  public function authorize(array $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    return (boolean) $this->auth;
  }

  public function resolve($root, $args)
  {
  	$list = new BoardList;
    $list->id = Board::generateId();
  	$list->name = $args['name'];
    $list->order = $args['order'];
    $list->board_id = $args['boardId'];
  	$list->save();

  	return BoardList::find($list->id);
  }
}