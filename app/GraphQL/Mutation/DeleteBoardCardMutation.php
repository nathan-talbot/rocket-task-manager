<?php 

namespace App\GraphQL\Mutation;

use GraphQL;
use App\BoardCard;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeleteBoardCardMutation extends Mutation
{
	protected $attributes = [
    'name' => 'deleteCard'
  ];

  public function type()
 	{
  	return GraphQL::type('BoardCard');
  }

  public function args()
  {
    return [
      'id' => ['name' => 'id', 'type' => Type::string()],
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'id' => ['required'],
    ];
  }

  public function authorize(array $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    return (boolean) $this->auth;
  }

  public function resolve($root, $args)
  {
  	$card = BoardCard::where('id', $args['id']);
  	$card->delete();
  	return $card;
  }
}