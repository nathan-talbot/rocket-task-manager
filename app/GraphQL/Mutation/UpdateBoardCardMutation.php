<?php 

namespace App\GraphQL\Mutation;

use GraphQL;
use App\BoardCard;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateBoardCardMutation extends Mutation
{
	protected $attributes = [
    'name' => 'updateCard'
  ];

  public function type()
 	{
  	return GraphQL::type('BoardCard');
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::string()
      ],
      'name' => [
        'name' => 'name',
        'type' => Type::string()
      ],
      'order' => [
        'name' => 'order',
        'type' => Type::int()
      ],
      'color' => [
        'name' => 'color',
        'type' => Type::string()
      ],
      'listId' => [
        'name' => 'listId',
        'type' => Type::string()
      ]
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'id' => ['required'],
    ];
  }

  public function authorize(array $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    return (boolean) $this->auth;
  }

  public function resolve($root, $args)
  {
    $card = BoardCard::where('id', $args['id'])->first();
    $card->name = isset($args['name']) ? $args['name'] : $card->name;
    $card->order = isset($args['order']) ? $args['order'] : $card->order;
    $card->color = isset($args['color']) ? $args['color'] : $card->color;
    $card->board_list_id = isset($args['listId']) ? $args['listId'] : $card->board_list_id;
    $card->save();
    return $card;
  }
}