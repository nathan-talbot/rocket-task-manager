<?php 

namespace App\GraphQL\Mutation;

use GraphQL;
use App\BoardList;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateBoardListMutation extends Mutation
{
	protected $attributes = [
    'name' => 'updateList'
  ];

  public function type()
 	{
  	return GraphQL::type('BoardList');
  }

  public function args()
  {
    return [
      'id' => [
        'name' => 'id',
        'type' => Type::nonNull(Type::string())
      ],
      'order' => [
        'name' => 'order',
        'type' => Type::nonNull(Type::int())
      ]
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'order' => ['required'],
    ];
  }

  public function authorize(array $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    return (boolean) $this->auth;
  }

  public function resolve($root, $args)
  {
    if (isset($args['id'])) {
  	  $list = BoardList::where('id', $args['id'])->first();
      $list->order = isset($args['order']) ? $args['order'] : $list->order;
      $list->save();
      return $list;
  	}
  }
}