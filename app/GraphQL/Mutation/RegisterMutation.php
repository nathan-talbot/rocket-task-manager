<?php 

namespace App\GraphQL\Mutation;

use GraphQL;
use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class RegisterMutation extends Mutation
{
	protected $attributes = [
    'name' => 'register'
  ];

  public function type()
 	{
  	return GraphQL::type('User');
  }

  public function args()
  {
    return [
      'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string())],
      'email' => ['name' => 'email', 'type' => Type::nonNull(Type::string())],
      'password' => ['name' => 'password', 'type' => Type::nonNull(Type::string())]
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'name' => ['required'],
      'email' => ['required'],
      'password' => ['required']
    ];
  }

  public function resolve($root, $args)
  {
    // Create new user
  	$user = new User;
    $user->user_id = User::generateId();
    $user->name = $args['name'];
    $user->email = $args['email'];
  	$user->password = Hash::make($args['password']);
  	$user->save();

    // Automatically login after
    $credentials = [
      'email' => $user->email, 
      'password' => $args['password']
    ];
    try {
      $token = JWTAuth::attempt($credentials);
    } catch (\JWTException $e) {
      $token = null;
    }
    if ($token != false) {
      $user_id = $user->user_id;
      $user_name = $user->name;
    } else {
      $token = null;
      $user_id = null;
      $user_name = null;
    }
    return [
      'token' => $token, 
      'user_id' => $user_id,
      'name' => $user_name
    ];
  }
}