<?php 

namespace App\GraphQL\Mutation;

use GraphQL;
use App\Board;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Tymon\JWTAuth\Facades\JWTAuth;

class AddBoardMutation extends Mutation
{
	protected $attributes = [
    'name' => 'addBoard'
  ];

  public function type()
 	{
  	return GraphQL::type('Board');
  }

  public function args()
  {
    return [
      'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string())],
      'user_id' => ['name' => 'user_id', 'type' => Type::nonNull(Type::string())]
    ];
  }

  public function rules(array $args = [])
  {
    return [
      'name' => ['required'],
      'user_id' => ['required'],
    ];
  }

  public function authorize(array $args)
  {
    try {
      $this->auth = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
      $this->auth = null;
    }
    return (boolean) $this->auth;
  }

  public function resolve($root, $args)
  {
  	$board = new Board;
    $board->name = $args['name'];
    $board->user_id = $args['user_id'];
  	$board->id = Board::generateId();
  	$board->save();
  	return Board::find($board->id);
  }
}